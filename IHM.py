# -*- coding: utf-8 -*-
def effaceEcran(image):
    image.close()

def affichageMenu():
    """Affiche les choix du menu"""
    pass

def ouvrirImage():
    print("Veuillez inserer l'image que vous voulez éditer.")
    from PIL import Image
    imageimportee = input("Entrer le nom de l'image : ")
    print()
    print('Votre image a bien été importée.')
    print()
    image = Image.open(imageimportee)
    return image
    
def afficherImage(image):
    image.show()

def afficherInfoImage(image):

    pass

def negatifImage(image):
    largeur = image.width
    hauteur = image.height
    nbr_colonne=0
    nbr_ligne=0
    for nbr_ligne in range (0,hauteur):
        for nbr_colonne in range (0,largeur):
            data=image.getpixel((nbr_colonne,nbr_ligne))
#                      R             G            B
            data2=(255-data[0],255-data[1],255-data[2])
            image.putpixel((nbr_colonne,nbr_ligne),(data2))
    image.show()
    
def decalageBGR(image):
    largeur = image.width
    hauteur = image.height
    nbr_colonne=0
    nbr_ligne=0
    for nbr_ligne in range (0,hauteur):
        for nbr_colonne in range (0,largeur):
            data=image.getpixel((nbr_colonne,nbr_ligne))
#                   B        G        R
            data2=(data[2],data[1],data[0])
            image.putpixel((nbr_colonne,nbr_ligne),(data2))
    image.show()
    
def decalageBRG(image):
    largeur = image.width
    hauteur = image.height
    nbr_colonne=0
    nbr_ligne=0
    for nbr_ligne in range (0,hauteur):
        for nbr_colonne in range (0,largeur):
            data=image.getpixel((nbr_colonne,nbr_ligne))
#                   B        R        G
            data2=(data[2],data[0],data[1])
            image.putpixel((nbr_colonne,nbr_ligne),(data2))
    image.show()

def decalageGBR(image):
    largeur = image.width
    hauteur = image.height
    nbr_colonne=0
    nbr_ligne=0
    for nbr_ligne in range (0,hauteur):
        for nbr_colonne in range (0,largeur):
            data=image.getpixel((nbr_colonne,nbr_ligne))
#                      G        B        R
            data2=(data[1],data[2],data[0])
            image.putpixel((nbr_colonne,nbr_ligne),(data2))
    image.show()
    
def decalageCouleur():
    print(color.RED + color.BOLD + 'Vous voici dans la section "Décalage des couleurs".'+ color.END)
    print()
    print('     Entrer "gbr" pour décaler les couleurs en GBR.')
    print('     Entrer "brg" pour décaler les couleurs en BRG.')
    print('     Entrer "bgr" pour décaler les couleurs en BGR.')
    print()
    print('     Entrer "q" pour quitter la section "Décalage des couleurs".')
    print()
    choix="0"
    while choix != "q":
        affichageMenu()
        choix=input(color.RED + color.BOLD + ' Choisissez une action : ' + color.END)
        if choix =="gbr":
            decalageGBR(image_imp)
        elif choix =="brg":
            decalageBRG(image_imp)
        elif choix =="bgr":
            decalageBGR(image_imp)
            pass
        else:
            pass
    print()
    
def RappelCommandes():
    print()
    print('    | Entrer "i" pour importer une nouvelle image.')
    print('    | Entrer "a" pour afficher votre image.')
    print('    | Entrer "n" pour inverser les couleur de votre image.')
    print('    | Entrer "c" pour accéder au menu "Changement des couleurs".')
    print('    | Entrer "r" pour effectuer une rotation de 90° à votre image.')
    print()
    print('      Entrer "q" pour quitter le logiciel')
    print()  
      
def rotationImage(image):
    print('Entrer le nombre pour effectuer la rotation de votre image.')
    rotate=int(input('Rotation de degré : '))
    print()
    image.rotate(rotate).show()

class color:
    BOLD = "\033[1m"
    YELLOW = "\033[93m"
    END = "\033[0m"
    GREEN = "\033[92m"
    RED = "\033[91m"
    PURPLE = "\033[95m"
print(color.BOLD, "Bienvenue dans ce logiciel de traitement d'image.", color.END)
print()
print('    | Entrer "i" pour importer une nouvelle image.')
print('    | Entrer "a" pour afficher votre image.')
print('    | Entrer "n" pour inverser les couleur de votre image.')
print('    | Entrer "c" pour accéder au menu "Changement des couleurs".')
print('    | Entrer "r" pour effectuer une rotation de 90° à votre image.')
print()
print('      Entrer "q" pour quitter le logiciel')
print()
choix="0"
while choix != "q":
    affichageMenu()
    print('Entrer "cmd" pour afficher le menu des commandes.')
    choix=input(color.RED + color.BOLD + ' Choisissez une action : ' + color.END)
    print()
    if choix=="i":
        image_imp=ouvrirImage()
    elif choix =="n":
        negatifImage(image_imp)
    elif choix =="c":
        decalageCouleur()
    elif choix =="cmd":
        RappelCommandes()
    elif choix =="a":
        afficherImage(image_imp)
    elif choix =="e":
        effaceEcran(image_imp)
    elif choix =="r":
        rotationImage(image_imp)
        pass
    else:
        pass