# -*- coding: utf-8 -*-
def effaceEcran():
    """Efface l'écran"""
    pass
def affichageMenu():
    """Affiche les choix du menu"""
    pass
def ouvrirImage():
    """Ouvre l'image demandéé"""
    pass
def afficherImage():
    """Affiche l'image chargée"""
    pass
def afficherInfoImage():
    """Affiche des informations sur l'image"""
    pass
def negatifImage():
    """Passe l'image en négtaif"""
    pass
def decalageCouleur():
    """Passage RGB->RVB etc..."""
    pass
def rotationImage():
    """Rotation image gauche/droite etc..."""
    pass
def symetrieImage():
    """symétrie axe verticale /horizontale"""
    pass
def Fin():
    """Quitter le programme"""
    pass
