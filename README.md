Ici nous pouvons retrouver les programmes et autres étapes 
contribuant au Mini-Projet d'ISN du Lycée André Malraux 2019
de Louis Georget et Maël Vassenet consistant à développer un 
logiciel de traitement d'image simple d'utilisation.

  • Maël travaillant sur le décalage colorimétrique de l'image.                                 
  • Louis travaillant sur le négatif de l'image.